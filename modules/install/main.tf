
locals {
  EXTRA_VALUES = {
  }
}

resource "helm_release" "release" {
  name       = "esphome"
  repository = "https://k8s-at-home.com/charts/"
  chart      = "esphome"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}